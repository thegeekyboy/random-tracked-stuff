### Setting up Oracle InstaClient for Linux

#### environment variables (.bashrc)

export ORACLE_BASE=/usr/lib/oracle
export ORACLE_HOME=/usr/lib/oracle/12.2/client64
export TNS_ADMIN=$ORACLE_HOME/network/admin

export ORACLE_USERNAME=ORAUSER
export ORACLE_PASSWORD=TEST1234
export ORACLE_SID=ORA_SID_NAME
export TWO_TASK=$ORACLE_SID

export PATH=$PATH:$ORACLE_HOME/bin:$ORACLE_HOME/lib

[[ -z "${LD_LIBRARY_PATH}" ]] && export LD_LIBRARY_PATH=/usr/lib:/usr/lib/oracle/12.2/client64/lib:/usr/lib/oracle/12.2/client64/lib || export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib:/usr/lib/oracle/12.2/client64/lib:/usr/lib/oracle/12.2/client64/lib

#### $ORACLE_HOME/network/admin/tnsnames.ora (sample)

ORA_SID_NAME=(
    DESCRIPTION=(
        ADDRESS=(PROTOCOL=TCP)(HOST=10.10.20.30)(PORT=1525)
    )
    (
        CONNECT_DATA=(SERVICE_NAME=ORA_SID_NAME)
    )
)

#### $ORACLE_HOME/network/admin/sqlnet.ora

NAMES.DIRECTORY_PATH=(TNSNAMES, EZCONNECT)
ADR_BASE = /usr/lib/oracle/grid
SQLNET.EXPIRE_TIME=2

DIAG_ADR_ENABLED=OFF
DIAG_SIGHANDLER_ENABLED=FALSE
DIAG_DDE_ENABLED=FALSE
